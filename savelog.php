<div class="container-list-absensi">
                <?php
                    foreach ($absensi_pegawai as $absenpegawai) :
                ?>
                    <div class="list-absensi">
                        <div class="row">
                            <div class="col-6">
                                <h4><?php echo date('l,d M Y', strtotime($absenpegawai['tgl_absen'])); ?></h4>
                            </div>
                            <div class="col-6">
                                <?php 
                                if($absenpegawai['status'] == "hadir"){
                                    echo "<div class='label-blue'>Hadir hari kerja</div>";
                                }
                                else if($absenpegawai['status'] == "tidak hadir"){
                                    echo "<div class='label-orange'>Tidak hadir</div>";
                                }
                                else{
                                    echo "<div class='label-orange'>Ijin</div>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>