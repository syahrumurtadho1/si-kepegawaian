<?= $this->extend('auth/templates/index'); ?>
<?= $this->section('content'); ?>
<?php 
    $session = session();
    $login = $session->getFlashdata('login');
    $username = $session->getFlashdata('email');
    $password = $session->getFlashdata('password');
?>
<section id="login-page">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 offset-xl-4 col-lg-4 offset-lg-4">
                <div class="hero">
                    <img src="<?= base_url(); ?>/img/illustrasi-login.png" alt="">
                </div>
                <div class="card-login">
                    <h4>Selamat Datang di,</h4>
                    <h2>Sistem Informasi Kepegawaian</h2>
                    <p>Silahkan masuk dengan akun yang sudah anda dimiliki</p>
                    <?php if($username){ ?>
                        <p style="color:red"><?php echo $username?></p>
                    <?php } ?>
                    
                    <?php if($password){ ?>
                        <p style="color:red"><?php echo $password?></p>
                    <?php } ?>
                    
                    <?php if($login){ ?>
                        <p style="color:green"><?php echo $login?></p>
                    <?php } ?>
                    <form class="form" method="post" action="/home/valid_login">
                        <input type="text" name="email" class="form-control" placeholder="Email" required>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                        <a href="<?= base_url(); ?>/pegawai"><button type="submit">Login Pegawai</button></a>
                        <p><a href="<?= base_url(); ?>/admin">Login Sebagai Admin</a></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection(); ?>
