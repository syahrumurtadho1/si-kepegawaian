<?php $session = session()?>
<?= $this->extend('pegawai/templates/index'); ?>
<?= $this->section('content'); ?>
<?php
    foreach ($datauser as $user) :
?>
<div class="container d-flex justify-content-end">
    <nav class="navbar navbar-expand-lg navbar-light">
        <ul class="navbar-nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $user['email']; ?>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/home/logout">Keluar</a>
            </div>
          </li>
    </nav>
</div>
<section id="pegawai-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="heading d-flex align-items-center justify-content-between">
                    <div>
                        <h4>Selamat Datang <?php echo $user['nama_pegawai']; ?>,</h4>
                        <h2>Sistem Informasi Kepegawaian</h2>
                    </div>
                    <div class="cta">
                        <button class="btn-blue" data-toggle="modal" data-target="#modalTambahPegawai"><i
                                class="uil uil-plus"></i>Tambahkan Pegawai</button>
                    </div>
                </div>
                <?php
                    foreach ($allpegawai as $pegawai) :
                ?>
                <div class="card-pegawai">
                    <div class="row">
                        <div class="col-lg-3">
                            <label>Nama Pegawai</label>
                            <h4><?php echo $pegawai['nama_pegawai']; ?></h4>
                        </div>
                        <div class="col-lg-3">
                            <label>Absensi Terpenuhi</label>
                            <h4><?php echo $pegawai['jumlah_absensi']; ?> / <?php echo cal_days_in_month(CAL_GREGORIAN,date('m'),date('y')) ?> Hari</h4>
                        </div>
                        <div class="col-lg-3">
                            <label>Jabatan</label>
                            <h4><?php echo $pegawai['jabatan']; ?></h4>
                        </div>
                        <div class="col-lg-3">
                            <div class="d-flex align-items-center justify-content-between">
                                <div>
                                    <label>Estimasi Gaji</label>
                                    <h4 class="text-green">Rp <?php echo number_format($pegawai['gaji_harian'] * $pegawai['jumlah_absensi']); ?></h4>
                                </div>
                                <div class="cta-view" 
                                data-id="<?= $pegawai['id_user']; ?>" 
                                data-nama="<?= $pegawai['nama_pegawai'];?>" 
                                data-jabatan="<?= $pegawai['jabatan'];?>" 
                                data-alamat="<?= $pegawai['alamat'];?>"
                                data-gaji="<?= number_format($pegawai['gaji_harian']); ?>"
                                data-telp="<?= $pegawai['no_telp']; ?>"
                                data-jmlabsen="<?= $pegawai['jumlah_absensi']; ?>">
                                    <i class="uil uil-eye"></i>
                                </div>
                                <div class="cta-edit" 
                                data-id="<?= $pegawai['id_user']; ?>" 
                                data-nama="<?= $pegawai['nama_pegawai'];?>" 
                                data-jabatan="<?= $pegawai['jabatan'];?>" 
                                data-alamat="<?= $pegawai['alamat'];?>"
                                data-gaji="<?= $pegawai['gaji_harian']; ?>"
                                data-telp="<?= $pegawai['no_telp']; ?>"
                                data-email="<?= $pegawai['email']; ?>"
                                data-password="<?= $pegawai['password']; ?>"
                                >
                                    <i class="uil uil-pen"></i>
                                </div>
                                <div class="cta-delete" 
                                data-id="<?= $pegawai['id_user']; ?>" >
                                    <i class="uil uil-trash"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- Modal Ijin -->
<div class="modal fade" id="modalTambahPegawai" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-between">
                <h2>Tambah Pegawai</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="uil uil-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <form action="/home/tambah_pegawai" method="post">
                <div class="form-pegawai">
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="text" name="nama_pegawai" class="form-control" placeholder="Nama Pegawai">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="jabatan" class="form-control" placeholder="Jabatan">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="alamat" class="form-control" placeholder="Alamat">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="no_telp" class="form-control" placeholder="Nomor Telp">
                        </div>
                        <div class="col-lg-12">
                            <input type="number" name="gaji_harian" class="form-control" placeholder="Gaji Harian">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="password" class="form-control" placeholder="Buat Password Baru">
                        </div>
                    </div>
                </div>
                <button class="btn-blue" type="submit">Tambahkan Pegawai</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEditPegawai" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-between">
                <h2>Edit Pegawai</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="uil uil-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <form action="/home/edit_user" method="post">
                <div class="form-pegawai">
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="text" name="nama_pegawai" class="edit_nama_user form-control" placeholder="Nama Pegawai">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="jabatan" class="edit_jabatan_user form-control" placeholder="Jabatan">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="alamat" class="edit_alamat_user form-control" placeholder="Alamat">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="no_telp" class="edit_telp_user form-control" placeholder="Nomor Telp">
                        </div>
                        <div class="col-lg-12">
                            <input type="number" name="gaji_harian" class="edit_gaji_user form-control" placeholder="Gaji Harian">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="email" class="edit_email form-control" placeholder="Email">
                        </div>
                        <div class="col-lg-6">
                            <input type="text" name="password" class="edit_password form-control" placeholder="Buat Password Baru">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id_user" class="edit_id_user">
                <button class="btn-blue" type="submit">Simpan Perubahan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
    <form action="/home/delete_user" method="post">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-end">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="uil uil-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-lg-12">
                        <h4>Apakah kamu yakin ingin menghapus user ini ?</h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id_user" class="userID">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
            </div>
        </div>
    </div>
    </form>
</div>
<!-- Modal Detail Pegawai -->
<div class="modal fade" id="modalDetailPegawai" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-end">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="uil uil-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="list-item">
                            <h4 class="nama_user">Syahru</h4>
                            <label class="jabatan_user">Sr.UI/UX Designer</label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="list-item">
                            <label>Total Absensi</label>
                            <h4><span class="jmlabsen_user"></span>/26 Hari</h4>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="list-item">
                            <label>Alamat</label>
                            <h4 class="alamat_user">Jl.Kalibokor Timur no.117 , Surabaya</h4>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="list-item">
                            <label>No Telp</label>
                            <h4 class="telp_user">0821-4314-5007</h4>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="list-item">
                            <label>Gaji Harian</label>
                            <h4 class="text-green gaji_user">Rp 300.000</h4>
                        </div>
                    </div>
                </div>
                <!-- <div class="heading-list-absensi">
                    <div class="row">
                        <div class="col-6">
                            <label>Tanggal</label>
                        </div>
                        <div class="col-6">
                            <label>Status</label>
                        </div>
                    </div>
                </div> -->
                
            </div>
        </div>
    </div>
</div>
<?php
    endforeach;
?>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<script>
        // get Edit Product
        $('.cta-view').click(function(event) {
            // get data from button edit
            const id = $(this).data('id');
            const absensi = $(this).data('absensi');
            const nama = $(this).data('nama');
            const jabatan = $(this).data('jabatan');
            const alamat = $(this).data('alamat');
            const gaji = $(this).data('gaji');
            const telp = $(this).data('telp');
            const jmlabsen = $(this).data('jmlabsen');
            // Set data to Form Edit
            $('.nama_user').text(nama);
            $('.jabatan_user').text (jabatan);
            $('.alamat_user').text (alamat);
            $('.gaji_user').text(gaji);
            $('.jmlabsen_user').text (jmlabsen);
            $('.telp_user').text (telp);
            $('#modalDetailPegawai').modal('show');
            
        });
        $('.cta-edit').on('click',function(){
            // get data from button edit
            const id = $(this).data('id');
            const nama = $(this).data('nama');
            const jabatan = $(this).data('jabatan');
            const alamat = $(this).data('alamat');
            const gaji = $(this).data('gaji');
            const telp = $(this).data('telp');
            const email = $(this).data('email');
            const password = $(this).data('password');
            // Set data to Form Edit
            $('.edit_id_user').val(id);
            $('.edit_nama_user').val(nama);
            $('.edit_jabatan_user').val(jabatan);
            $('.edit_alamat_user').val (alamat);
            $('.edit_gaji_user').val(gaji);
            $('.edit_email').val (email);
            $('.edit_password').val (password);
            $('.edit_telp_user').val (telp);
            // Call Modal Edit
            $('#modalEditPegawai').modal('show');
        });
        // get Delete Product
        $('.cta-delete').on('click',function(){
            // get data from button edit
            const id = $(this).data('id');
            // Set data to Form Edit
            $('.userID').val(id);
            // Call Modal Edit
            $('#modalDelete').modal('show');
        });
</script>
<?= $this->endSection(); ?>