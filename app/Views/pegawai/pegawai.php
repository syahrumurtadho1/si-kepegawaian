<?php $session = session()?>
<?= $this->extend('pegawai/templates/index'); ?>
<?= $this->section('content'); ?>
<?php
    foreach ($datapegawai as $user) :
?>
<div class="container d-flex justify-content-end">
    <nav class="navbar navbar-expand-lg navbar-light">
        <ul class="navbar-nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $user['email']; ?>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/home/logout">Keluar</a>
            </div>
          </li>
    </nav>
</div>
<section id="pegawai-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="heading d-flex align-items-center justify-content-between">
                    <div>
                        <h4>Selamat Datang <?php echo $user['nama_pegawai']; ?>,</h4>
                        <h2>Sistem Informasi Kepegawaian</h2>
                    </div>
                    <div class="cta">
                        <!-- <button class="btn-orange" data-toggle="modal" data-target="#modalIzin"><i
                                class="uil uil-envelope-edit"></i>Pengajuan Izin</button> -->
                       
                        <button class="btn-blue" data-toggle="modal" data-target="#modalAbsensi"><i
                                class="uil uil-user-check"></i>Absen Sekarang</button>
                    </div>
                </div>
                <?php
                    foreach ($absensi as $absen) :
                ?>
                <div class="card-pegawai">
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Tanggal Absen</label>
                            <h4><?php echo date('l,d M Y', strtotime($absen['tgl_absen'])); ?></h4>
                        </div>
                        <div class="col-lg-4">
                            <label>Status</label>
                            <?php 
                            if($absen['status'] == "hadir"){
                                echo "<div class='label-blue'>Hadir hari kerja</div>";
                            }
                            else if($absen['status'] == "tidak hadir"){
                                echo "<div class='label-orange'>Tidak hadir</div>";
                            }
                            else{
                                echo "<div class='label-orange'>Ijin</div>";
                            }
                            ?>
                            
                        </div>
                        <div class="col-lg-2">
                            <label>Jam Masuk</label>
                            <h4><?php echo $absen['jam_masuk']; ?></h4>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<!-- Modal Absensi -->
<div class="modal fade" id="modalAbsensi" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-end">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="uil uil-times"></i>
                </button>
            </div>
            <div class="modal-body">
            <form action="/home/save_absensi" method="post">
            <input type="hidden" name="jumlah_absensi" value="<?php echo $user['jumlah_absensi']; ?>">
            <input type="hidden" name="id_user" value="<?php echo $user['id_user']; ?>">
            <input type="hidden" name="status" value="hadir">
            <input type="hidden" name="tgl_absen" value="<?php echo date('ymd'); ?>">
            <input type="hidden" name="jam_masuk" value="<?php date_default_timezone_set('Asia/Jakarta'); echo date('H:i:s'); ?>">
            
                <div class="list-item">
                    <label>Tanggal Absen</label>
                    <h4><?php echo date('l,d M Y'); ?></h4>
                </div>
                <div class="list-item">
                    <label>Waktu Saat Ini</label>
                    <h4><?php date_default_timezone_set('Asia/Jakarta'); echo date('H:i:s'); ?></h4>
                </div>
                <button type="submit" class="btn-blue">Absen Sekarang</button>
            </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Ijin -->
<div class="modal fade" id="modalIzin" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-between">
                <h2>Pengajuan Izin</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="uil uil-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-izin">
                    <label>Tanggal Izin</label>
                    <input type="date" class="form-control">
                    <textarea class="form-control" rows="3" placeholder="Alasan Izin"></textarea>
                </div>
                <button class="btn-blue">Ajukan Izin</button>
                <div class="title">
                    <h4>Status Perizinan</h4>
                </div>
                <div class="list-item">
                    <div class="row">
                        <div class="col-lg-8">
                            <label>Tanggal Izin</label>
                            <h4>24 Agustus 2021</h4>
                        </div>
                        <div class="col-lg-4">
                            <label>Status</label>
                            <div class="label-blue">Disetujui</div>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="row">
                        <div class="col-lg-8">
                            <label>Tanggal Izin</label>
                            <h4>24 Agustus 2021</h4>
                        </div>
                        <div class="col-lg-4">
                            <label>Status</label>
                            <div class="label-orange">Ditolak</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    endforeach;
?>
<?= $this->endSection(); ?>