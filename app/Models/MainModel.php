<?php

namespace App\Models;

use CodeIgniter\Model;

class MainModel extends Model
{
    // protected $table = 'user';
    public function get_profile($id) {
        return $this->db->table('data_user')
        ->join('user','user.id_user=data_user.id_user')
        ->where('data_user.id_user',$id)
        ->get()->getResultArray();
    }

    public function get_all_pegawai() {
        return $this->db->table('data_user')
        ->join('user','user.id_user=data_user.id_user')
        ->get()->getResultArray();
    }
    // public function get_absensi_pegawai($idpegawai) {
    //     return $this->db->table('data_user')
    //     ->join('user','user.id_user=data_user.id_user')
    //     ->join('absensi','user.id_user=data_user.id_user')
    //     ->orderBy('tgl_absen', 'desc')
    //     ->where('data_user.id_user',$idpegawai)
    //     ->get()->getResultArray();
    // }
    public function get_absensi($id) {
        return $this->db->table('data_user')
        ->join('user','user.id_user=data_user.id_user')
        ->join('absensi','user.id_user=data_user.id_user')
        ->orderBy('tgl_absen', 'desc')
        ->where('absensi.id_user',$id)
        ->where('user.id_user',$id)
        ->get()->getResultArray();
    }

    public function save_absensi($data){
        $query = $this->db->table('absensi')->insert($data);
        return $query;
    }
    public function tambahUser($data){
        $query = $this->db->table('user')->insert($data);
        return $this->db->insertID();
    }
    public function tambahPegawai($dataPegawai){
        $query = $this->db->table('data_user')->insert($dataPegawai);
        return $query;
    }
    public function update_jumlah_absensi($data_update,$iduser){
        $query = $this->db->table('data_user')->update($data_update,array('id_user' => $iduser));
        return $query;
    }
    public function count_absensi($id,$tgl_absen){
        $sql="select count(tgl_absen) as jumlah_absen from absensi where id_user=$id and tgl_absen=$tgl_absen";    
        $query = $this->db->query($sql);
        return $query->getResult();
    }
    public function edit_user($data, $id)
    {
        $query = $this->db->table('user')->update($data, array('id_user' => $id));
        return $query;
    }
    public function edit_pegawai($dataPegawai, $id)
    {
        $query = $this->db->table('data_user')->update($dataPegawai, array('id_user' => $id));
        return $query;
    }
    public function delete_user($id)
    {
        $query = $this->db->table('absensi')->delete(array('id_user' => $id));
        $query = $this->db->table('data_user')->delete(array('id_user' => $id));
        $query = $this->db->table('user')->delete(array('id_user' => $id));
        return $query;
    } 
    
}

?>