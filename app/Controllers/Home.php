<?php

namespace App\Controllers;
use App\Models\UserModel;
use App\Models\MainModel;

class Home extends BaseController
{
	public function __construct()
    {
		//membuat user model untuk konek ke database 
        $this->userModel = new UserModel();
        
        //meload validation
        $this->validation = \Config\Services::validation();
        
        //meload session
        $this->session = \Config\Services::session();
        $this->session = session();
		
    }
	public function index()
	{
		//cek role dari session
        if($this->session->get('role') == "admin"){
            return redirect()->to('/admin');
        }
		else if($this->session->get('role') == "pegawai"){
            return redirect()->to('/pegawai');
        }
        else{
			return view('auth/login');
		}
	}
	public function pegawai()
	{
		//cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/');
        }

		//cek role dari session
		if($this->session->get('role') != "pegawai"){
			return redirect()->to('/admin');
		}
        
        $id = $this->session->get('id_user');
        $mainModel = new MainModel();
        $data['datapegawai'] = $mainModel->get_profile($id);
        $data['absensi'] = $mainModel->get_absensi($id);
        // $data['allpegawai'] = $mainModel->get_all_pegawai();
        
        return view('pegawai/pegawai', $data);
	}
	public function admin()
	{
		//cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/');
        }
        
        //cek role dari session
        if($this->session->get('role') != "admin"){
            return redirect()->to('/pegawai');
        }
        
        $id = $this->session->get('id_user');
        $idpegawai = $this->request->getPost('id_user');
        $mainModel = new MainModel();
        $data['datauser'] = $mainModel->get_profile($id);
        $data['allpegawai'] = $mainModel->get_all_pegawai();
        // $data['absensi_pegawai'] = $mainModel->get_absensi_pegawai($idpegawai);

		return view('admin/admin', $data);
	}

	public function valid_login()
    {
        //ambil data dari form
        $data = $this->request->getPost();
        
        //ambil data user di database yang usernamenya sama 
        $user = $this->userModel->where('email', $data['email'])->first();
        
        //cek apakah username ditemukan
        if($user){
            //cek password
            //jika salah arahkan lagi ke halaman login
            if($user['password'] != $data['password']){
                session()->setFlashdata('password', 'Password salah');
                return redirect()->to('/');
            }
            else{
                //jika benar, arahkan user masuk ke aplikasi 
                $sessLogin = [
                    'isLogin' => true,
                    'id_user' => $user['id_user'],
                    'email' => $user['email'],
                    'role' => $user['role']
                    ];
                $this->session->set($sessLogin);
                return redirect()->to('/pegawai');
            }
        }
        else{
            //jika username tidak ditemukan, balikkan ke halaman login
            session()->setFlashdata('email', 'Email tidak ditemukan');
            return redirect()->to('/');
        }
    }

    public function save_absensi()
    {
        $model = new MainModel();
        date_default_timezone_set('Asia/Jakarta');
        $id = $this->session->get('id_user');
        $tgl_absen = $this->request->getPost('tgl_absen');
        $datacount['countabsen'] = $model->count_absensi($id,$tgl_absen);
        // echo print_r($datacount['countabsen']);
            foreach($datacount['countabsen'] as $datac){
                if(($datac->jumlah_absen)>=1){
                    echo "<script>
                        alert('Kamu sudah absen hari ini');
                        window.location.href='/pegawai';
                        
                        </script>";
                }
                else{
                    $data = array(
                            'id_user' => $this->request->getPost('id_user'),
                            'tgl_absen' => $this->request->getPost('tgl_absen'),
                            'status'    => $this->request->getPost('status'),
                            'jam_masuk' => $this->request->getPost('jam_masuk'),
                    );
                    $iduser = $this->request->getPost('id_user');
                    $jumlah_absensi = $this->request->getPost('jumlah_absensi');
                    $countdatenow = cal_days_in_month(CAL_GREGORIAN,date('m'),date('y'));
                    if($jumlah_absensi >= $countdatenow){
                        $data_update = array(
                            'id_user' => $this->request->getPost('id_user'),
                            'jumlah_absensi' => 0,
                        );
                    }
                    else{
                        $data_update = array(
                            'id_user' => $this->request->getPost('id_user'),
                            'jumlah_absensi' => ($this->request->getPost('jumlah_absensi')+1),
                        );
                    }
                    $model->save_absensi($data);
                    $model->update_jumlah_absensi($data_update,$iduser);
                    return redirect()->to('/pegawai');
                        
                }
            }
        }
        public function tambah_pegawai()
        {
            $model = new MainModel();
            $data = array(
                'email'        => $this->request->getPost('email'),
                'password'       => $this->request->getPost('password'),
            );
            $idUser = $model->tambahUser($data);
            $dataPegawai = array(
                'id_user' => $idUser,
                'nama_pegawai' => $this->request->getPost('nama_pegawai'),
                'alamat' => $this->request->getPost('alamat'),
                'jabatan' => $this->request->getPost('jabatan'),
                'no_telp' => $this->request->getPost('no_telp'),
                'gaji_harian' => $this->request->getPost('gaji_harian'),
            );
            $model->tambahPegawai($dataPegawai);
            return redirect()->to('/admin');
        }
        public function delete_user()
        {
            $model = new MainModel();
            $id = $this->request->getPost('id_user');
            $model->delete_user($id);
            return redirect()->to('/admin');
        }
        public function edit_user()
        {
            $model = new MainModel();
            $id = $this->request->getPost('id_user');
            $user = array(
                'email'        => $this->request->getPost('email'),
                'password'       => $this->request->getPost('password'),
            );
            $model->edit_user($user, $id);
            $dataPegawai = array(
                'nama_pegawai'        => $this->request->getPost('nama_pegawai'),
                'jabatan'       => $this->request->getPost('jabatan'),
                'alamat'       => $this->request->getPost('alamat'),
                'no_telp'       => $this->request->getPost('no_telp'),
                'gaji_harian'       => $this->request->getPost('gaji_harian'),
            );
            $model->edit_pegawai($dataPegawai, $id);
            return redirect()->to('/admin');
        }
        public function logout()
    {
        //hancurkan session 
        //balikan ke halaman login
        $this->session->destroy();
		return view('auth/login');
    }
}
